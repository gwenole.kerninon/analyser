package analyzer.data;

import java.util.HashMap;
import java.util.Map;

public final class Cache {

    private final Map<String, AnalyzerResult> cache = new HashMap<>();

    private final static Cache INSTANCE = new Cache();

    public static Cache getInstance() {
        return INSTANCE;
    }

    private Cache() {}

    public void put(String key, AnalyzerResult result) {
        this.cache.put(key, result);
    }

    public AnalyzerResult get(String key) {
        return this.cache.get(key);
    }
}
