package analyzer.data;

import static analyzer.data.AnalyzerConstant.*;

public class AnalyzerResult {

    private int nbAdenine = 0;
    private int nbCytosine = 0;
    private int nbGuanine = 0;
    private int nbUracile = 0;
    private int nbThymine = 0;

    private String displaySequence = "";

    private final MoleculeType type;
    private boolean fromCache = false;

    public AnalyzerResult(MoleculeType type) {
        this.type = type;
    }

    public AnalyzerResult(AnalyzerResult analyserResult) {
        this.nbAdenine = analyserResult.nbAdenine;
        this.nbCytosine = analyserResult.nbCytosine;
        this.nbGuanine = analyserResult.nbGuanine;
        this.nbUracile = analyserResult.nbUracile;
        this.nbThymine = analyserResult.nbThymine;
        this.type = analyserResult.type;
    }

    public int getNbAdenine() {
        return nbAdenine;
    }

    public void setNbAdenine(int nbAdenine) {
        this.nbAdenine = nbAdenine;
    }

    public void incrementNbAdenine() {
        this.nbAdenine++;
    }

    public int getNbCytosine() {
        return nbCytosine;
    }

    public void incrementNbCytosine() {
        this.nbCytosine++;
    }

    public int getNbGuanine() {
        return nbGuanine;
    }

    public void incrementNbGuanine() {
        this.nbGuanine++;
    }

    public int getNbUracile() {
        return nbUracile;
    }

    public void incrementNbUracile() {
        this.nbUracile++;
    }

    public int getNbThymine() {
        return nbThymine;
    }

    public void incrementNbThymine() {
        this.nbThymine++;
    }

    public boolean isFromCache() {
        return fromCache;
    }

    public void setFromCache(boolean fromCache) {
        this.fromCache = fromCache;
    }

    @Override
    public String toString() {
        String result = "Adenine : " + nbAdenine +
                "\nCytosine : " + nbCytosine +
                "\nGuanine : " + nbGuanine;

        result += switch (this.type) {
            case ADN -> "\nThymine : " + nbThymine;
            case ARN -> "\nUracile : " + nbUracile;
            default -> "";
        };

        return result;
    }

    public void setDisplaySequence(String display) {
        this.displaySequence =display;
    }

    public String getDisplaySequence() {
        return displaySequence;
    }
}
