package analyzer.data;

public final class AnalyzerConstant {

    private AnalyzerConstant(){
        throw new UnsupportedOperationException();
    }

    public enum MoleculeType {
        ARN,
        ADN
    }

    public static final char URACILE = 'U';
    public static final char THYMINE = 'T';
    public static final char ADENINE = 'A';
    public static final char CYTOSINE = 'C';
    public static final char GUANINE = 'G';

}
