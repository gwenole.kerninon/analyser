package analyzer;


import analyzer.engine.Analyzer;
import analyzer.factory.AnalyzerEngineFactory;
import analyzer.factory.AnalyzerEngineFactoryException;

import java.util.Scanner;

public class Main {

    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Saisie utilisateur initiale
        String molecule = getNewMoleculeString();

        while (!molecule.equalsIgnoreCase("STOP")) {

            // Analyse
            try {
                Analyzer analyserEngine = AnalyzerEngineFactory.getAnalyzerEngine(molecule, true);
                System.out.println(analyserEngine.analyze(molecule));
            } catch (AnalyzerEngineFactoryException e) {
                System.out.println(e.getMessage());
                System.out.println("La chaine saisie était : " + e.getMoleculeToAnalyze());
            }

            // Nouvelle saisie utilisateur
            molecule = getNewMoleculeString();
        }
    }

    private static String getNewMoleculeString() {
        System.out.println("Veuillez saisir la chaine de la molécule à analyser");
        return sc.next();
    }
}
