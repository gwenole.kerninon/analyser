package analyzer.engine;

import analyzer.data.AnalyzerResult;
import analyzer.data.Cache;

public final class CacheableProxyAnalyzer implements Analyzer {

    private final Analyzer analyzer;

    private final Cache cache;

    public CacheableProxyAnalyzer(Analyzer analyzer) {
        this.analyzer = analyzer;
        this.cache = Cache.getInstance();
    }

    @Override
    public AnalyzerResult analyze(String moleculeString) {
        AnalyzerResult result;
        if(cache.get(moleculeString) != null) {
            result = cache.get(moleculeString);
            System.out.println("*" + result.getDisplaySequence() + "*");
            result.setFromCache(true);
        } else {
            result = analyzer.analyze(moleculeString);
            cache.put(moleculeString, result);
        }
        return result;
    }
}
