package analyzer.engine;

import analyzer.data.AnalyzerResult;
import analyzer.displayer.Displayer;

import static analyzer.data.AnalyzerConstant.*;

public class ADNAnalyzer extends AbstractAnalyzer {

    public ADNAnalyzer() {
    }

    public ADNAnalyzer(Displayer displayer) {
        this.displayer = displayer;
        this.moleculeType = MoleculeType.ADN;
    }

    @Override
    protected AnalyzerResult incrementResult(char letter, AnalyzerResult result) {
        AnalyzerResult resultUpdated = new AnalyzerResult(result);
        switch (letter) {
            case ADENINE -> resultUpdated.incrementNbAdenine();
            case CYTOSINE -> resultUpdated.incrementNbCytosine();
            case GUANINE -> resultUpdated.incrementNbGuanine();
            case THYMINE -> resultUpdated.incrementNbThymine();
            default -> throw new RuntimeException("La chaine ADN doit uniquement être composée des lettres A,C,G,T");
        }
        return resultUpdated;
    }

}
