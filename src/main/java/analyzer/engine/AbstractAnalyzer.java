package analyzer.engine;

import analyzer.data.AnalyzerResult;
import analyzer.displayer.Displayer;

import static analyzer.data.AnalyzerConstant.*;

public abstract class AbstractAnalyzer implements Analyzer {

    protected Displayer displayer;

    protected MoleculeType moleculeType;

    public AnalyzerResult analyze(String arn) {
        AnalyzerResult result = new AnalyzerResult(this.moleculeType);
        StringBuilder display = new StringBuilder();
        if (arn != null) {
            char[] letters = arn.toUpperCase().toCharArray();
            for (char letter : letters) {
                if (displayer != null) {
                    display.append(displayer.displayCharacter(letter));
                }
                result = incrementResult(letter, result);
            }
        }
        result.setDisplaySequence(display.toString());
        System.out.println(display);
        return result;
    }

    protected abstract AnalyzerResult incrementResult(char letter, AnalyzerResult result);

}
