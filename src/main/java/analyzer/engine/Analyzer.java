package analyzer.engine;

import analyzer.data.AnalyzerResult;

public interface Analyzer {
    AnalyzerResult analyze(String moleculeString);
}
