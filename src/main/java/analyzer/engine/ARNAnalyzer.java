package analyzer.engine;

import analyzer.data.AnalyzerResult;
import analyzer.displayer.Displayer;

import static analyzer.data.AnalyzerConstant.*;

public class ARNAnalyzer extends AbstractAnalyzer {

    public ARNAnalyzer() {
    }

    public ARNAnalyzer(Displayer displayer) {
        this.displayer = displayer;
        this.moleculeType = MoleculeType.ARN;
    }

    @Override
    protected AnalyzerResult incrementResult(char letter, AnalyzerResult result) {
        AnalyzerResult resultUpdated = new AnalyzerResult(result);
        switch (letter) {
            case ADENINE -> resultUpdated.incrementNbAdenine();
            case CYTOSINE -> resultUpdated.incrementNbCytosine();
            case GUANINE -> resultUpdated.incrementNbGuanine();
            case URACILE -> resultUpdated.incrementNbUracile();
            default -> throw new RuntimeException("La chaine ARN doit uniquement être composée des lettres A,C,G,U");
        }
        return resultUpdated;
    }

}
