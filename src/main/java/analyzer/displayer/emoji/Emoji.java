package analyzer.displayer.emoji;

/**
 * Example to use with System.out.print :
 * <br>
 * <br> System.out.print(Color.BLACK_BOLD);
 * <br> System.out.println("Black_Bold");
 * <br> System.out.print(Color.RESET);
 */
enum Emoji {

    WINK("\uD83D\uDE09"),
    BLUSH("\uD83D\uDE0A"),
    YUM("\uD83D\uDE0B"),
    RELIEVED("\uD83D\uDE0C"),
    HEART_EYES("\uD83D\uDE0D"),
    FEARFUL("\uD83D\uDE28");

    private final String code;

    Emoji(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}