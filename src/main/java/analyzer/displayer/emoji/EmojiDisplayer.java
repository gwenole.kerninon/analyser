package analyzer.displayer.emoji;

import analyzer.displayer.Displayer;

import static analyzer.data.AnalyzerConstant.*;

public class EmojiDisplayer implements Displayer {

    @Override
    public String displayCharacter(char c) {
        var emoji = switch (c) {
            case ADENINE -> Emoji.BLUSH;
            case CYTOSINE -> Emoji.HEART_EYES;
            case GUANINE -> Emoji.RELIEVED;
            case URACILE -> Emoji.WINK;
            case THYMINE -> Emoji.YUM;
            default -> Emoji.FEARFUL;
        };
        return emoji.toString();
    }

}
