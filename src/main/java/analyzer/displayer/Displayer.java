package analyzer.displayer;

public interface Displayer {
    String displayCharacter(char c);
}
