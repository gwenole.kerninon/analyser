package analyzer.displayer.color;

/**
 * Example to use with System.out.print :
 * <br>
 * <br> System.out.print(Color.BLACK_BOLD);
 * <br> System.out.println("Black_Bold");
 * <br> System.out.print(Color.RESET);
 */
enum Color {
    //Color end string, color reset
    RESET("\033[0m"),

    BLACK_BACKGROUND("\033[40m"),   // BLACK
    GREEN_BACKGROUND("\033[42m"),   // GREEN
    YELLOW_BACKGROUND("\033[43m"),  // YELLOW
    BLUE_BACKGROUND("\033[44m"),    // BLUE
    MAGENTA_BACKGROUND("\033[45m"), // MAGENTA
    CYAN_BACKGROUND("\033[46m");    // CYAN

    private final String code;

    Color(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}