package analyzer.displayer.color;

import analyzer.displayer.Displayer;
import static analyzer.data.AnalyzerConstant.*;

public class ColorDisplayer implements Displayer {

    @Override
    public String displayCharacter(char c) {
        var color = switch (c) {
            case ADENINE -> Color.BLUE_BACKGROUND;
            case CYTOSINE -> Color.CYAN_BACKGROUND;
            case GUANINE -> Color.GREEN_BACKGROUND;
            case URACILE -> Color.MAGENTA_BACKGROUND;
            case THYMINE -> Color.YELLOW_BACKGROUND;
            default -> Color.BLACK_BACKGROUND;
        };
        return color + " " + Color.RESET;
    }
}
