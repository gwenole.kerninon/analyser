package analyzer.factory;

public class AnalyzerEngineFactoryException extends Exception {

    private String molecule;

    private static final String MESSAGE = "Aucun analyseur défini pour cette molecule";

    public AnalyzerEngineFactoryException() {
        super(MESSAGE);
    }
    public AnalyzerEngineFactoryException(String molecule) {
        super(MESSAGE);
        this.molecule = molecule;
    }

    public String getMoleculeToAnalyze() {
        return this.molecule;
    }
}
