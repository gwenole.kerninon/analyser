package analyzer.factory;

import analyzer.displayer.color.ColorDisplayer;
import analyzer.displayer.emoji.EmojiDisplayer;
import analyzer.engine.ADNAnalyzer;
import analyzer.engine.ARNAnalyzer;
import analyzer.engine.Analyzer;
import analyzer.engine.CacheableProxyAnalyzer;

import static analyzer.data.AnalyzerConstant.*;

public final class AnalyzerEngineFactory {

    public static Analyzer getAnalyzerEngine(String molecule, boolean withCache) throws AnalyzerEngineFactoryException {
        Analyzer analyzer = null;
        if (molecule != null) {
            if (molecule.contains(String.valueOf(URACILE))) {
                System.out.println("Instanciation du moteur spécifique ARN");
                analyzer = new ARNAnalyzer(new ColorDisplayer());
            } else if (molecule.contains(String.valueOf(THYMINE))) {
                System.out.println("Instanciation du moteur spécifique ADN");
                analyzer = new ADNAnalyzer(new EmojiDisplayer());
            } else {
                throw new AnalyzerEngineFactoryException(molecule);
            }
        } else {
            throw new AnalyzerEngineFactoryException();
        }
        return withCache ? new CacheableProxyAnalyzer(analyzer) : analyzer;
    }

}
