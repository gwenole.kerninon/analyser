package analyzer.engine.mock;

import analyzer.displayer.Displayer;

public class MockDisplayer implements Displayer {
    @Override
    public String displayCharacter(char c) {
        return "X";
    }
}