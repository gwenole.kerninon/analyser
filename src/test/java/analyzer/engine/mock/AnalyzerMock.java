package analyzer.engine.mock;

import analyzer.data.AnalyzerResult;
import analyzer.engine.Analyzer;

public class AnalyzerMock implements Analyzer {

    private AnalyzerResult result;

    public AnalyzerMock(AnalyzerResult result){
        this.result = result;
    }

    @Override
    public AnalyzerResult analyze(String moleculeString) {
        return result;
    }
}
