package analyzer.engine;

import analyzer.data.AnalyzerResult;
import analyzer.engine.mock.MockDisplayer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class ADNAnalyzerTest {

    private static ADNAnalyzer adnAnalyzer;

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
        adnAnalyzer = new ADNAnalyzer();
    }

    @Test
    void testWithEmptyString() {
        AnalyzerResult result = adnAnalyzer.analyze("");
        assertEquals(0, result.getNbAdenine());
        assertEquals(0, result.getNbCytosine());
        assertEquals(0, result.getNbGuanine());
        assertEquals(0, result.getNbThymine());
    }

    @Test
    void testWithNullString() {
        AnalyzerResult result = adnAnalyzer.analyze(null);
        assertEquals(0, result.getNbAdenine());
        assertEquals(0, result.getNbCytosine());
        assertEquals(0, result.getNbGuanine());
        assertEquals(0, result.getNbThymine());
    }

    @Test()
    void testWithInvalidARNStringV1() {
        try {
            adnAnalyzer.analyze("ATGCUATGCCGGTTT");
            fail("Une RuntimeException aurait du être remontée");
        } catch (RuntimeException e) {
            String expectedMessage = "La chaine ADN doit uniquement être composée des lettres A,C,G,T";
            String actualMessage = e.getMessage();

            assertEquals(expectedMessage, actualMessage);
        }
    }

    @Test()
    void testWithInvalidARNStringV2() {
        Exception exception = assertThrows(RuntimeException.class, () -> adnAnalyzer.analyze("ATGCUATGCCGGTTT"));

        String expectedMessage = "La chaine ADN doit uniquement être composée des lettres A,C,G,T";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void testWithValidARNString() {
        AnalyzerResult result = adnAnalyzer.analyze("ATGCATGCCGGTTT");
        assertEquals(2, result.getNbAdenine());
        assertEquals(3, result.getNbCytosine());
        assertEquals(4, result.getNbGuanine());
        assertEquals(5, result.getNbThymine());
    }

    @Test
    void testWithValidARNStringAndDisplayer() {
        Analyzer analyzer = new ADNAnalyzer(new MockDisplayer());
        AnalyzerResult result = analyzer.analyze("ATGCATGCCGGTTT");
        assertEquals(2, result.getNbAdenine());
        assertEquals(3, result.getNbCytosine());
        assertEquals(4, result.getNbGuanine());
        assertEquals(5, result.getNbThymine());
    }


    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }
}
