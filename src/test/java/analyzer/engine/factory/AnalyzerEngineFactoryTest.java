package analyzer.engine.factory;

import analyzer.engine.ADNAnalyzer;
import analyzer.engine.ARNAnalyzer;
import analyzer.engine.Analyzer;
import analyzer.engine.CacheableProxyAnalyzer;
import analyzer.factory.AnalyzerEngineFactory;
import analyzer.factory.AnalyzerEngineFactoryException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AnalyzerEngineFactoryTest {

    @Test
    void testCreateARNAnalyzer() throws AnalyzerEngineFactoryException {
        Analyzer analyzer = AnalyzerEngineFactory.getAnalyzerEngine("AUGCAUGCCGGUUU", false);
        assertTrue(analyzer instanceof ARNAnalyzer);
    }

    @Test
    void testCreateADNAnalyzer() throws AnalyzerEngineFactoryException {
        Analyzer analyzer = AnalyzerEngineFactory.getAnalyzerEngine("ATGCATGCCGGTTT", false);
        assertTrue(analyzer instanceof ADNAnalyzer);
    }

    @Test
    void testCreateARNAnalyzerWithCache() throws AnalyzerEngineFactoryException {
        Analyzer analyzer = AnalyzerEngineFactory.getAnalyzerEngine("AUGCAUGCCGGUUU", true);
        assertTrue(analyzer instanceof CacheableProxyAnalyzer);
    }

    @Test
    void testCreateADNAnalyzerWithCache() throws AnalyzerEngineFactoryException {
        Analyzer analyzer = AnalyzerEngineFactory.getAnalyzerEngine("ATGCATGCCGGTTT", true);
        assertTrue(analyzer instanceof CacheableProxyAnalyzer);
    }

    @Test
    void testUnknownAnalyzer() {
        String molecule = "AGC";
        try {
            AnalyzerEngineFactory.getAnalyzerEngine(molecule, false);
            fail("Une exception AnalyzerEngineFactoryException doit être remontée");
        } catch (AnalyzerEngineFactoryException e) {
            assertEquals(e.getMessage(), "Aucun analyseur défini pour cette molecule");
            assertEquals(e.getMoleculeToAnalyze(), molecule);
        }
    }

    @Test
    void testUnknownAnalyzerWithNull() {
        String molecule = null;
        try {
            AnalyzerEngineFactory.getAnalyzerEngine(molecule, false);
            fail("Une exception AnalyzerEngineFactoryException doit être remontée");
        } catch (AnalyzerEngineFactoryException e) {
            assertEquals(e.getMessage(), "Aucun analyseur défini pour cette molecule");
            assertEquals(e.getMoleculeToAnalyze(), molecule);
        }
    }

}
