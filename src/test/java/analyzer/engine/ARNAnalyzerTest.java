package analyzer.engine;

import analyzer.data.AnalyzerResult;
import analyzer.engine.mock.MockDisplayer;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class ARNAnalyzerTest {

    private static ARNAnalyzer arnAnalyzer;

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
        arnAnalyzer = new ARNAnalyzer();
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
    }

    @Test
    void testWithEmptyString() {
        AnalyzerResult result = arnAnalyzer.analyze("");
        assertEquals(0, result.getNbAdenine());
        assertEquals(0, result.getNbCytosine());
        assertEquals(0, result.getNbGuanine());
        assertEquals(0, result.getNbUracile());
    }

    @Test
    void testWithNullString() {
        AnalyzerResult result = arnAnalyzer.analyze(null);
        assertEquals(0, result.getNbAdenine());
        assertEquals(0, result.getNbCytosine());
        assertEquals(0, result.getNbGuanine());
        assertEquals(0, result.getNbUracile());
    }

    @Test
    void testWithInvalidARNStringV1() {
        try {
            arnAnalyzer.analyze("AUGCTAUGCCGGUUU");
            fail("Une RuntimeException aurait du être remontée");
        } catch(RuntimeException e) {
            String expectedMessage = "La chaine ARN doit uniquement être composée des lettres A,C,G,U";
            String actualMessage = e.getMessage();

            assertEquals(expectedMessage, actualMessage);
        }
    }

    @Test
    void testWithInvalidARNStringV2() {
        Exception exception = assertThrows(RuntimeException.class, () -> arnAnalyzer.analyze("AUGCTAUGCCGGUUU"));

        String expectedMessage = "La chaine ARN doit uniquement être composée des lettres A,C,G,U";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void testWithValidARNString() {
        AnalyzerResult result = arnAnalyzer.analyze("AUGCAUGCCGGUUU");
        assertEquals(2, result.getNbAdenine());
        assertEquals(3, result.getNbCytosine());
        assertEquals(4, result.getNbGuanine());
        assertEquals(5, result.getNbUracile());
    }

    @Test
    void testWithValidARNStringAndDisplayer() {
        Analyzer analyzer = new ARNAnalyzer(new MockDisplayer());
        AnalyzerResult result = analyzer.analyze("AUGCAUGCCGGUUU");
        assertEquals(2, result.getNbAdenine());
        assertEquals(3, result.getNbCytosine());
        assertEquals(4, result.getNbGuanine());
        assertEquals(5, result.getNbUracile());
    }


    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests");
    }

}
