package analyzer.engine;

import analyzer.data.AnalyzerConstant;
import analyzer.data.AnalyzerResult;
import analyzer.engine.mock.AnalyzerMock;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CacheableProxyAnalyzerTest {

    @Test
    void testWithNoCache() {
        AnalyzerResult result = new AnalyzerResult(AnalyzerConstant.MoleculeType.ARN);
        result.setNbAdenine(1);
        CacheableProxyAnalyzer cacheableProxyAnalyzer = new CacheableProxyAnalyzer(new AnalyzerMock(result));
        AnalyzerResult res1 = cacheableProxyAnalyzer.analyze("A");
        assertEquals(res1.getNbAdenine(), result.getNbAdenine());
        assertFalse(res1.isFromCache());
    }

    @Test
    void testWithCache() {
        AnalyzerResult result = new AnalyzerResult(AnalyzerConstant.MoleculeType.ARN);
        result.setNbAdenine(1);

        CacheableProxyAnalyzer cacheableProxyAnalyzer = new CacheableProxyAnalyzer(new AnalyzerMock(result));

        AnalyzerResult res1 = cacheableProxyAnalyzer.analyze("B");
        assertEquals(res1.getNbAdenine(), result.getNbAdenine());
        assertFalse(res1.isFromCache());

        cacheableProxyAnalyzer.analyze("B");
        assertEquals(res1.getNbAdenine(), result.getNbAdenine());
        assertTrue(res1.isFromCache());
    }

}
