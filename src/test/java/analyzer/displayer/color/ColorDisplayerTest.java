package analyzer.displayer.color;

import analyzer.displayer.color.ColorDisplayer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static analyzer.data.AnalyzerConstant.*;

public class ColorDisplayerTest {

    private static ColorDisplayer colorDisplayer;

    @BeforeAll
    public static void initAll() {
        colorDisplayer = new ColorDisplayer();
    }

    @Test
    public void testDisplayCharAdenine() {
        assertEquals(colorDisplayer.displayCharacter(ADENINE), "\u001B[44m \u001B[0m");
    }

    @Test
    public void testDisplayCharCytosine() {
        assertEquals(colorDisplayer.displayCharacter(CYTOSINE), "\u001B[46m \u001B[0m");
    }

    @Test
    public void testDisplayCharGuanine() {
        assertEquals(colorDisplayer.displayCharacter(GUANINE), "\u001B[42m \u001B[0m");
    }

    @Test
    public void testDisplayCharUracile() {
        assertEquals(colorDisplayer.displayCharacter(URACILE), "\u001B[45m \u001B[0m");
    }

    @Test
    public void testDisplayCharThymine() {
        assertEquals(colorDisplayer.displayCharacter(THYMINE), "\u001B[43m \u001B[0m");
    }

    @Test
    public void testDisplayCharEmpty() {
        assertEquals(colorDisplayer.displayCharacter(' '), "\u001B[40m \u001B[0m");
    }

    @Test
    public void testDisplayCharOther() {
        assertEquals(colorDisplayer.displayCharacter('O'), "\u001B[40m \u001B[0m");
    }
}
