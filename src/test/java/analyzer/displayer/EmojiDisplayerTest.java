package analyzer.displayer;

import analyzer.displayer.emoji.EmojiDisplayer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static analyzer.data.AnalyzerConstant.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmojiDisplayerTest {

    private static EmojiDisplayer emojiDisplayer;

    @BeforeAll
    public static void initAll() {
        emojiDisplayer = new EmojiDisplayer();
    }

    @Test
    public void testDisplayCharAdenine() {
        assertEquals(emojiDisplayer.displayCharacter(ADENINE), "\uD83D\uDE0A");
    }

    @Test
    public void testDisplayCharCytosine() {
        assertEquals(emojiDisplayer.displayCharacter(CYTOSINE), "\uD83D\uDE0D");
    }

    @Test
    public void testDisplayCharGuanine() {
        assertEquals(emojiDisplayer.displayCharacter(GUANINE), "\uD83D\uDE0C");
    }

    @Test
    public void testDisplayCharUracile() {
        assertEquals(emojiDisplayer.displayCharacter(URACILE), "\uD83D\uDE09");
    }

    @Test
    public void testDisplayCharThymine() {
        assertEquals(emojiDisplayer.displayCharacter(THYMINE), "\uD83D\uDE0B");
    }

    @Test
    public void testDisplayCharEmpty() {
        assertEquals(emojiDisplayer.displayCharacter(' '), "\uD83D\uDE28");
    }

    @Test
    public void testDisplayCharOther() {
        assertEquals(emojiDisplayer.displayCharacter('O'), "\uD83D\uDE28");
    }
}
